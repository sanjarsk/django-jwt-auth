from .base import *  # noqa

# Database connection settings
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bittyin',
        'USER': 'bittyin',
        'PASSWORD': 'bittyin1234',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}

ALLOWED_HOSTS = ['localhost']
