import datetime

import jwt
from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db import models


class UserManager(BaseUserManager):
    def create_user(self, username, email, password=None):
        if username is None:
            raise TypeError('Username is required')

        if email is None:
            raise TypeError('Email address is required')

        user = self.model(username=username, email=self.normalize_email(email))

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(username=username, email=email, password=password)
        user.role = User.ADMIN
        user.is_staff = True
        user.save(using=self._db)

        return user


class User(AbstractBaseUser):
    username = models.CharField(db_index=True, max_length=40, unique=True)
    email = models.EmailField(unique=True)
    phone_number = models.CharField(max_length=20, blank=True)
    last_login = models.DateTimeField(null=True, blank=True)
    language = models.CharField(max_length=3)
    verified_email = models.BooleanField(default=False)
    verified_phone = models.BooleanField(default=False)
    verified_id = models.BooleanField(default=False)
    wallet = models.CharField(max_length=250)
    password_change_date = models.DateField(null=True, blank=True)

    ADMIN = 'admin'
    USER = 'user'
    role = models.CharField(max_length=5, choices=((ADMIN, 'Admin'), (USER, 'User')), default=USER)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # expected by Django
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    objects = UserManager()

    # which field to use for logging in
    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    def __str__(self):
        return self.username

    @property
    def token(self):
        return self._generate_jwt_token()

    def _generate_jwt_token(self):
        dt = datetime.now() + datetime.timedelta(days=60)

        token = jwt.encode({
            'id': self.pk,
            'exp': int(dt.strftime('%s'))
        }, settings.SECRET_KEY, algorithm='HS256')

        return token.decode('utf-8')
