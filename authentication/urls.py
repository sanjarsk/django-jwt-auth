from django.urls import path
from authentication.views import Login, Signup, AuthInfo, PasswordChange

urlpatterns = [
    path('login', Login.as_view()),
    path('signup', Signup.as_view()),
    path('authInfo', AuthInfo.as_view()),
    path('resetPassword', PasswordChange.as_view())
]
