import json

from django.conf import settings
from django.contrib.auth.admin import sensitive_post_parameters_m
from rest_framework.generics import RetrieveUpdateAPIView, GenericAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.views import APIView
from rest_framework import status, generics, permissions
from rest_framework.response import Response
from datetime import datetime

from rest_framework_jwt.serializers import RefreshJSONWebTokenSerializer, VerifyJSONWebTokenSerializer, \
    JSONWebTokenSerializer
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.views import JSONWebTokenAPIView

from authentication.models import User
from authentication.serializers import UserCreateSerializer, UserSerializer, UserDetailSerializer, \
    PasswordChangeSerializer

jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class CustomJSONWebTokenAPIView(JSONWebTokenAPIView):
    """
    Base API View that various JWT interactions inherit from.
    """
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            from rest_framework_jwt.utils import jwt_response_payload_handler
            response_data = jwt_response_payload_handler(token, user, request)
            response = Response(
                headers={'Authentication': 'Bearer {}'.format(response_data.get('token'))}
            )

            return response

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class Login(CustomJSONWebTokenAPIView):
    """
    API View that receives a POST with a user's username and password.
    Returns a JSON Web Token that can be used for authenticated requests.
    """
    serializer_class = JSONWebTokenSerializer


    # def post(self, request):
    #     user = request.data.get('user', {})
    #
    #     # Notice here that we do not call `serializer.save()` like we did for
    #     # the registration endpoint. This is because we don't  have
    #     # anything to save. Instead, the `validate` method on our serializer
    #     # handles everything we need.
    #     serializer = self.serializer_class(data=user)
    #     serializer.is_valid(raise_exception=True)
    #
    #     return Response(serializer.data, status=status.HTTP_200_OK)


class CustomVerifyJSONWebToken(CustomJSONWebTokenAPIView):
    """
    API View that checks the veracity of a token, returning the token if it
    is valid.
    """
    serializer_class = VerifyJSONWebTokenSerializer


class CustomRefreshJSONWebToken(CustomJSONWebTokenAPIView):
    """
    API View that returns a refreshed token (with new expiration) based on
    existing token
    If 'orig_iat' field (original issued-at-time) is found, will first check
    if it's within expiration window, then copy it to the new token
    """
    serializer_class = RefreshJSONWebTokenSerializer


class Signup(APIView):
    """
    Endpoint to register new user.
    """
    permission_classes = (AllowAny,)

    def post(self, request):
        # validation
        serializer = UserCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        model_serializer = UserSerializer(data=serializer.data)
        model_serializer.is_valid(raise_exception=True)
        user = model_serializer.save()
        output_serializer = UserSerializer(instance=user)

        return Response(output_serializer.data, status=status.HTTP_201_CREATED)


class AuthInfo(RetrieveUpdateAPIView):
    serializer_class = UserDetailSerializer
    permission_classes = (IsAuthenticated,)

    def get_object(self):
        return self.request.user


class PasswordChange(GenericAPIView):
    """
    Calls Django Auth SetPasswordForm save method.
    Accepts the following POST parameters: new_password1, new_password2
    Returns the success/fail message.
    """
    serializer_class = PasswordChangeSerializer
    permission_classes = (IsAuthenticated,)

    # @sensitive_post_parameters_m
    # def dispatch(self, *args, **kwargs):
    #     return super(PasswordChange, self).dispatch(*args, **kwargs)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response({'msg': 'success'}, status.HTTP_204_NO_CONTENT)
