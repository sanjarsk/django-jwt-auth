Custom JWT Authentication
=======

### Create virtual environment for a project:
```
$ pyenv virtualenv <env-name>

```

### If you use PyCharm, in settings show the interpreter *(the interpreter used in virtual environment created)* 
### PyCharm will always activate it for you
___


### To install needed packages run:
___
```
$ pip install -r requirements/local.txt
```

### Add local.py file with development settings in settings folder using `local_settings_example.txt` file
___



### Before commiting please pass code through linter:
___
```
$ flake8
```